import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/services/carrito.service';
import { Carrito } from 'src/app/model/Carrito';
import { FechaPromocional } from 'src/app/model/FechaPromocional';
import { TipoCarrito } from 'src/app/model/TipoCarrito';

@Component({
  selector: 'app-carrito-list',
  templateUrl: './carrito-list.component.html',
  styleUrls: ['./carrito-list.component.css']
})
export class CarritoListComponent implements OnInit {

  carritos: any;
  currentCarrito = null;
  currentIndex = -1;
  title = '';
  fechasPromocionales: FechaPromocional[] = new Array<FechaPromocional>();

  constructor(private carritoService: CarritoService) {

    let f1 = new FechaPromocional();
    f1.desde = new Date('December 1, 2019 00:00:00');
    f1.hasta = new Date('March 1, 2020 23:59:00');
    let f2 = new FechaPromocional();
    f2.desde = new Date('June 15, 2020 00:00:00');
    f2.hasta = new Date('July 1, 2020 23:59:00');

    this.fechasPromocionales.push(f1);
    this.fechasPromocionales.push(f2);

  }

  ngOnInit(): void {
    this.retrieveCarritos();
  }

  retrieveCarritos() {
    this.carritoService.getCarritos()
      .subscribe(
        (data: Carrito[]) => {
          data.forEach(c => {
            this.fechasPromocionales.forEach(f => {
              if ((c.fechaCreacion <= f.hasta && c.fechaCreacion >= f.desde)) {
                let tipoCarrito = new TipoCarrito();
                tipoCarrito.id = 2;
                tipoCarrito.descripcion = 'FECHA ESPECIAL';
                c.tipoCarrito = tipoCarrito;
              }
            });
            let fechaC = new Date(c.fechaCreacion);
            if ((fechaC.getFullYear() != new Date().getFullYear() || fechaC.getMonth() != new Date().getMonth() || fechaC.getDay() != new Date().getDay()) && !c.fechaCompra)
            {
              this.carritoService.delete(c.id).subscribe(x => {
              });
            }
            c.productos.forEach(p => {
              c.total = c.total + p.precio;
            });
            if (c.productos.length == 5) {
              c.total = 80 * 100 / c.total;
            } else if (c.productos.length > 10) {
              if (c.tipoCarrito.id = 1) {
                c.total = c.total - 200;
              } else if (c.tipoCarrito.id = 2) {
                c.total = c.total - 500;
              } else if (c.tipoCarrito.id = 3) {
                c.total = c.total - 700;
                let m = c.productos[0]
                c.productos.forEach(prdto => {
                  if (prdto.precio < m.precio) {
                    m = prdto;
                  }
                });
                c.total = c.total - m.precio;
              }
            }
          });
          this.carritos = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList() {
    this.retrieveCarritos();
    this.currentCarrito = null;
    this.currentIndex = -1;
  }

  setActiveCarrito(carrito, index) {
    this.currentCarrito = carrito;
    this.currentIndex = index;
  }

  borrarCarrito(carrito, index) {
    this.carritoService.delete(carrito.id).subscribe(x => {
      this.refreshList();
    });
  }

  finalizarCompra(carrito: Carrito) {
    carrito.fechaCompra = new Date();
    this.carritoService.update(carrito.id, carrito).subscribe(x => {
      this.refreshList();
    })
  }
}