Enunciado Carrito de compra

El objetivo del ejercicio es simular una aplicación de carrito de compras implementando las principales funcionalidades.

Existen tres tipos de carritos, el común, el promocionable por fecha especial y el promocionable por usuario vip.
Cuando el usuario solicita crear un nuevo carrito la aplicación determina el carrito correspondiente y no se combinan las promociones.   
(Un carrito NO puede ser VIP y Promocionable por fecha especial por ejemplo)
 
Se debe poder crear un carrito nuevo, eliminar un carrito, agregar productos al carrito, eliminarlos y consultar el estado del carrito que nos daría el total a pagar. No hace falta modelar el pago sino simplemente que quede guardado con los items correspondientes.
 
La idea es que exista algún parámetro en el sistema para poder simular la fecha. Si el cliente no finalizó la compra, el carrito automáticamente se destruye. En el caso que haya completado la compra, debe quedar registrado. El cliente puede realizar varias compras en el mismo día.
 
No es necesario desarrollar el ABM de los productos, las fechas promocionables, ni de los usuarios, con el hecho de tenerlos en la base de datos cargados para 
funcionar es suficiente.
 
Tampoco es necesario generar el frontend con la sesión del usuario. Lo ideal sería tener un frontend para simular la aplicación enviando parámetros y pudiendo visualizar el correcto funcionamiento de los request. Por ejemplo, agregar cantidades de un producto al carrito de un cliente, eliminar cierta cantidad de productos, eliminar producto, eliminar carrito, finalizar compra del carrito, consultar clientes VIP, clientes que pasaron a ser VIP en un determinado mes, clientes que dejaron de ser VIP en un determinado mes.
 
Para calcular el valor del carrito se debe tener en cuenta: 
 
Si se compran exactamente 5 productos:
- Se hace un descuento general del 20%.
 
Si se compran más de 10 productos:
- Si el carrito es común se hará un descuento de $200.
- Si el carrito es promocionable por fecha especial se hace un descuento general de $500.
- Si el carrito es vip, se bonifica el producto más barato y se hace un descuento general de $700.

Acceso cliente VIP
- Si el cliente en un determinado mes, realizó compras por más de $10.000 netos, pasa a ser considerado VIP en su próxima compra. (considerar el valor de lo que realmente paga el cliente por los carritos luego de aplicarle los descuentos)
- Si el cliente en un determinado mes, no realizó compras, deja de ser VIP si lo era.
  
La aplicación deberá estar hecha en JAVA, con Hibernate (configuración en xml o anotaciones es indistinto). Se pide que alguna de las funcionalidades, se exponga como servicio web REST. En cuanto a la base de datos, es indistinto, mientras sea una base de datos relacional.
