package com.carritocompra.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.carritocompra.model.entity.Carrito;

public interface iCarritoDao extends CrudRepository<Carrito, Long> {

}
